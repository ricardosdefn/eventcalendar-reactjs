import React from 'react';

import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import swal from 'sweetalert';
import {Modal, Button} from 'react-bootstrap'

export default class EventCalendar extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        events: [],
        newEvent: {
          id: 1,
          title: 'Evento',
          start: '2020-03-25T19:30',
          end: '2020-03-25T23:00',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mattis facilisis magna sed aliquam.'
        },
        selectedEvent: {
          id: -1,
          title: '',
          start: '',
          end: '',
          extendedProps: {
            description: ''
          }
        },
        showEventModal: false
      };

      this.handleNewEventSubmit = this.handleNewEventSubmit.bind(this);
      this.handleNewEventTitleChange = this.handleNewEventTitleChange.bind(this);
      this.handleNewEventStartChange = this.handleNewEventStartChange.bind(this);
      this.handleNewEventEndChange = this.handleNewEventEndChange.bind(this);
      this.handleNewEventDescriptionChange = this.handleNewEventDescriptionChange.bind(this);
      this.handleEventClick = this.handleEventClick.bind(this);
      this.handleDeleteEvent = this.handleDeleteEvent.bind(this);
      this.handleEventModalShow = this.handleEventModalShow.bind(this);
      this.handleEventModalClose = this.handleEventModalClose.bind(this);
  }

  handleNewEventSubmit(e) {
    this.setState({
      events: [
        ...this.state.events,
        {
          id: this.state.newEvent.id,
          title: this.state.newEvent.title,
          start: this.state.newEvent.start,
          end: this.state.newEvent.end,
          description: this.state.newEvent.description
        }
      ]
    });

    this.setState({
      newEvent: {
        id: this.state.newEvent.id + 1,
        title: '',
        start: '',
        end: ''
      }
    });

    swal("Pronto!", "Evento adicionado com sucesso!", "success");

    e.preventDefault();
  }

  handleNewEventTitleChange(e) {
    let newEvent = this.state.newEvent;
    newEvent.title = e.target.value;
    this.setState({ newEvent: newEvent });
  }

  handleNewEventStartChange(e) {
    let newEvent = this.state.newEvent;
    newEvent.start = e.target.value;
    this.setState({ newEvent: newEvent });
  }

  handleNewEventEndChange(e) {
    let newEvent = this.state.newEvent;
    newEvent.end = e.target.value;
    this.setState({ newEvent: newEvent });
  }

  handleNewEventDescriptionChange(e) {
    let newEvent = this.state.newEvent;
    newEvent.description = e.target.value;
    this.setState({ newEvent: newEvent });
  }

  handleEventClick(e) {
    let selectedEvent = e.event;
    console.log(selectedEvent);
    this.setState({ selectedEvent: selectedEvent });
    this.handleEventModalShow();
  }

  handleDeleteEvent() {
    let eventos = [...this.state.events];
    let indice = eventos.findIndex(o => o.id == this.state.selectedEvent.id);
    let evento = eventos[indice];

    console.log(evento);

    if (evento) {
      swal({
        title: "Tem certeza?",
        text: "Deseja realmente deletar o evento \"" + evento.title + "\"?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          eventos.splice(indice, 1);

          let selectedEvent = {
            id: -1,
            title: '',
            start: '',
            end: '',
            extendedProps: {
              description: ''
            }
          }

          this.setState({ events: eventos, selectedEvent: selectedEvent });

          this.handleEventModalClose();
          swal("Pronto!", "O evento foi excluído com sucesso!", "success");
        }
      });
    }
  }

  handleEventModalShow() { this.setState({ showEventModal: true }); }
  handleEventModalClose() { this.setState({ showEventModal: false }); }

  formatDateTime(dt) {
    const date = new Date(dt);
    let y = date.getFullYear();
    let n = date.getMonth();
    if (n < 10) { n = '0' + n; }
    let d = date.getDay();
    if (d < 10) { d = '0' + d; }
    let h = date.getHours();
    if (h < 10) { h = '0' + h; }
    let m = date.getMinutes();
    if (m < 10) { m = '0' + m; }


    return `${d}/${n}/${y} às ${h}:${m}`;
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-4 mb-5">
          <form onSubmit={this.handleNewEventSubmit}>
            <h3 className="text-uppercase">Novo evento</h3>
            <div className="form-group">
              <label>Título do Evento</label>
              <input
                type="text"
                className="form-control"
                placeholder="Digite aqui"
                value={this.state.newEvent.title}
                onChange={this.handleNewEventTitleChange}
                required />
            </div>
            <div className="form-group">
              <label>Início do Evento</label>
              <input
                type="datetime-local"
                className="form-control"
                placeholder="Digite aqui"
                value={this.state.newEvent.start}
                onChange={this.handleNewEventStartChange}
                required />
            </div>
            <div className="form-group">
              <label>Fim do Evento</label>
              <input
                type="datetime-local"
                className="form-control"
                placeholder="Digite aqui"
                value={this.state.newEvent.end}
                onChange={this.handleNewEventEndChange}
                required />
            </div>
            <div className="form-group">
              <label>Descrição</label>
              <textarea
                className="form-control"
                placeholder="Descreva os detalhes do evento"
                onChange={this.handleNewEventDescriptionChange}
                style={{height: "100px", resize: "none"}}
                required>
                  {this.state.newEvent.description}
              </textarea>
            </div>
            <button type="submit" className="btn btn-primary">Criar Evento</button>
          </form>
        </div>
        <div className="col-md-8 mb-5">
          <FullCalendar
            defaultView="dayGridMonth"
            events={this.state.events}
            locale="pt-br"
            eventTimeFormat={{
              hour: 'numeric',
              minute: '2-digit',
              meridiem: false
            }}
            eventClick={this.handleEventClick}
            plugins={[ dayGridPlugin ]} />
        </div>
        <div className="col-12">
          <Modal show={this.state.showEventModal} onHide={this.handleEventModalClose} animation={false}>
            <Modal.Header closeButton>
              <Modal.Title>{ this.state.selectedEvent.title }</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="row">
                <div className="col-md-6">
                  <strong>Início: </strong>
                  { this.formatDateTime(this.state.selectedEvent.start) }
                </div>
                <div className="col-md-6">
                  <strong>Fim: </strong>
                  { this.formatDateTime(this.state.selectedEvent.end) }
                </div>
              </div>
              <p><strong>Descrição: </strong>{this.state.selectedEvent.extendedProps.description}</p>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleEventModalClose}>
                Fechar
              </Button>
              <Button variant="danger" onClick={this.handleDeleteEvent}>
                Deletar
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    )
  }
}
