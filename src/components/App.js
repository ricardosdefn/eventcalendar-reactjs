import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';

import EventCalendar from './EventCalendar'

export default class App extends React.Component {
  render() {
    return (
      <main className="container py-5">
        <EventCalendar />
      </main>
    )
  }
}
